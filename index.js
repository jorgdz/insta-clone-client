'use strict'

const Client = require('./lib/client')

exports.createClient = (options) => {
  return new Client(options)
}
