'use strict'

const fetch = require('node-fetch')
const Promise = require('bluebird')

class Client {
  constructor (options) {
    this.options = options || {
      endpoints: {
        pictures: 'http://api.insta-clone.com/picture',
        users: 'http://api.insta-clone.com/user',
        auth: 'http://api.insta-clone.com/auth'
      }
    }
  }

  async getPicture (id, callback) {
    var response = await fetch(`${this.options.endpoints.pictures}/${id}`)
    var body = await response.json()
    return Promise.resolve(body).asCallback(callback)
  }

  async savePicture (picture, token, callback) {
    var opts = {
      method: 'POST',
      body: JSON.stringify(picture),
      headers: { 'Content-Type': 'application/json', Authorization: `Bearer ${token}` }
    }

    var response = await fetch(`${this.options.endpoints.pictures}/`, opts)
    var body = await response.json()
    return Promise.resolve(body).asCallback(callback)
  }

  async likePicture (id, username, callback) {
    var user = { username: username }
    var options = {
      method: 'POST',
      body: JSON.stringify(user),
      headers: { 'Content-Type': 'application/json' }
    }

    var response = await fetch(`${this.options.endpoints.pictures}/${id}/like`, options)
    var body = await response.json()
    return Promise.resolve(body).asCallback(callback)
  }

  async dislikePicture (id, username, callback) {
    var user = { username: username }
    var options = {
      method: 'POST',
      body: JSON.stringify(user),
      headers: { 'Content-Type': 'application/json' }
    }

    var response = await fetch(`${this.options.endpoints.pictures}/${id}/dislike`, options)
    var body = await response.json()
    return Promise.resolve(body).asCallback(callback)
  }

  async listPictures (callback) {
    var response = await fetch(`${this.options.endpoints.pictures}/list`)
    var body = await response.json()
    return Promise.resolve(body).asCallback(callback)
  }

  async listPicturesByTag (tag, callback) {
    var response = await fetch(`${this.options.endpoints.pictures}/tag/${tag}`)
    var body = await response.json()
    return Promise.resolve(body).asCallback(callback)
  }

  async saveUser (user, callback) {
    var opts = {
      method: 'POST',
      body: JSON.stringify(user),
      headers: { 'Content-Type': 'application/json' }
    }

    var response = await fetch(`${this.options.endpoints.users}/`, opts)
    var body = await response.json()
    return Promise.resolve(body).asCallback(callback)
  }

  async getUser (username, callback) {
    var response = await fetch(`${this.options.endpoints.users}/${username}`)
    var body = await response.json()
    return Promise.resolve(body).asCallback(callback)
  }

  async authenticate (username, password, callback) {
    var user = { username: username, password: password }
    var opts = {
      method: 'POST',
      body: JSON.stringify(user),
      headers: { 'Content-Type': 'application/json' }
    }

    try {
      var response = await fetch(`${this.options.endpoints.auth}/`, opts)
      var body = await response.json()
      return Promise.resolve(body).asCallback(callback)
    } catch (err) {
      return Promise.reject(new Error('Invalid credentials')).asCallback(callback)
    }
  }
}

module.exports = Client
