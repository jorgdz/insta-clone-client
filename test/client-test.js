'use strict'

const test = require('ava')
const instagram = require('../')
const fixtures = require('./fixtures')
const nock = require('nock')

var options = {
  endpoints: {
    pictures: 'http://insta-clone.test/picture',
    users: 'http://insta-clone.test/user',
    auth: 'http://insta-clone.test/auth'
  }
}

test.beforeEach(t => {
  t.context.client = instagram.createClient(options)
})

test('client', t => {
  const client = t.context.client

  t.is(typeof client.getPicture, 'function')
  t.is(typeof client.savePicture, 'function')
  t.is(typeof client.likePicture, 'function')
  t.is(typeof client.listPictures, 'function')
  t.is(typeof client.listPicturesByTag, 'function')
  t.is(typeof client.saveUser, 'function')
  t.is(typeof client.getUser, 'function')
  t.is(typeof client.authenticate, 'function')
})

test('getPicture', async t => {
  const client = t.context.client

  var image = fixtures.getImage()

  nock(options.endpoints.pictures)
    .get(`/${image.publicId}`)
    .reply(200, image)

  var result = await client.getPicture(image.publicId)

  t.deepEqual(image, result)
})

test('savePicture', async t => {
  const client = t.context.client

  var token = 'xxx-xxx-xxx'
  var image = fixtures.getImage()

  var newImage = {
    url: image.url,
    description: image.description
  }

  nock(options.endpoints.pictures, {
    reqheaders: {
      Authorization: `Bearer ${token}`
    }
  })
    .post('/', newImage)
    .reply(201, image)

  var result = await client.savePicture(newImage, token)

  t.deepEqual(image, result)
})

test('likePicture', async t => {
  const client = t.context.client

  var image = fixtures.getImage()
  image.liked = true
  image.likes = 1

  var user = {
    username: image.userId
  }

  nock(options.endpoints.pictures)
    .post(`/${image.publicId}/like`, user)
    .reply(200, image)

  var result = await client.likePicture(image.publicId, image.userId)

  t.deepEqual(image, result)
})

test('listPictures', async t => {
  const client = t.context.client

  var images = fixtures.getImages(3)

  nock(options.endpoints.pictures)
    .get('/list')
    .reply(200, images)

  var result = await client.listPictures()

  t.deepEqual(images, result)
})

test('listPicturesByTag', async t => {
  const client = t.context.client

  var images = fixtures.getImages(3)
  var tag = 'instagram'

  nock(options.endpoints.pictures)
    .get(`/tag/${tag}`)
    .reply(200, images)

  var result = await client.listPicturesByTag(tag)

  t.deepEqual(images, result)
})

test('saveUser', async t => {
  const client = t.context.client

  var user = fixtures.getUser()
  var newUser = {
    username: user.username,
    name: user.name,
    email: 'jdzm@outlook.es',
    password: '12345'
  }

  nock(options.endpoints.users)
    .post('/', newUser)
    .reply(201, user)

  var result = await client.saveUser(newUser)

  t.deepEqual(result, user)
})

test('getUser', async t => {
  const client = t.context.client

  var user = fixtures.getUser()

  nock(options.endpoints.users)
    .get(`/${user.username}`)
    .reply(200, user)

  var result = await client.getUser(user.username)

  t.deepEqual(result, user)
})

test('auth', async t => {
  const client = t.context.client

  var credentials = {
    username: 'jorgdz',
    password: '12345'
  }

  var token = 'xxx-xxx-xxx'

  nock(options.endpoints.auth)
    .post('/', credentials)
    .reply(200, { token: token })

  var result = await client.authenticate(credentials.username, credentials.password)
  t.deepEqual(result.token, token)
})
